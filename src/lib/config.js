const SITE_ABOUT = "An immersive story about a mysterious virtual reality application where you play as one the last humans being hunted to extinction"
const SITE_NAME = "The Grand Challenge"
const SITE_TITLE = SITE_NAME

const EPISODES = [
  {
    "name": "Episode 0 (Prologue) - The Stars Beyond Our Reach", 
    "date": "2024-02-22",
    "desc": "If only I could be as free as the heavenly bodies above...", 
    "imageUrl": "/episodes/tgc-ep0.png",
    "link": "https://ep0.tgc.atos.to/"
  },
]

const SOCIALS_FOOTER = {
  "KofiMonthly": "https://ko-fi.com/osco/tiers",
  "KofiSingle": "https://ko-fi.com/osco",
  "Instagram": "http://www.instagram.com/Atemosta",
  "OscoSubscribe": "https://www.osco.blog/subscribe",
  "Patreon": "https://patreon.com/osco",
  "Reddit": "https://www.reddit.com/r/Atemosta/",
  "Twitter": "http://www.twitter.com/Atemosta",
  "YouTube": "http://www.youtube.com/@Atemosta",
}

// -- TESTING: UNDER DEVELOPMENT -- // 
const BADGES_TEST = [
  {
    "title": "The Stars Beyond Our Reach - Received for completing the Prologue",
    "imageUrl": "/episodes/tgc-ep0.png"
  },
]

const OPENINGS = [
  {
    "name": "Opening 0", 
    "desc": "🎵 H-el-ical - clea-rly 🎵", 
    "link": "https://op0.tgc.atos.to/"
  },
]

const SOCIALS_OLD = [
  {"name": "Mastodon", "desc": "Open-source microblogging platform (Twitter alternative)", "link": "https://mastodon.social/@Atemostaody"},
  {"name": "Odysee", "desc": "Open-source video platform (YouTube alternative)", "link": "https://odysee.com/@Atemosta"},
  {"name": "Revolt", "desc": "Open-source messaging platform (Discord alternative)", "link": "https://app.revolt.chat/invite/S2H3wGSq"},
]

const SOCIALS = [
  {
    "name": "Mastodon (make an account here for free for a limited time!)", 
    "desc": "@osco@debaucherytea.party", 
    "link": "https://debaucherytea.party/@osco"
  },
  {
    "name": "Twitter (X)", 
    "desc": "@oscoDOTblog", 
    "link": "https://twitter.com/oscoDOTblog"
  },
  {
    "name": "YouTube", 
    "desc": "@oscoDOTblog", 
    "link": "https://www.youtube.com/@oscoDOTblog"
  },
]

const SUBSCRIPTIONS = [
  {
    "name": "Ko-fi ☕", 
    "desc": "Pay with Card, Paypal, or Venmo!", 
    "link": "https://ko-fi.com/osco/tiers"
  },
]

export {
  BADGES_TEST,
  EPISODES,
  OPENINGS, 
  SITE_ABOUT,
  SITE_NAME,
  SITE_TITLE,
  SOCIALS,
  SOCIALS_FOOTER,
  SUBSCRIPTIONS
}