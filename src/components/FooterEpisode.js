import React from 'react'
import NewsletterSignup from './NewsletterSignup';
import Link from 'next/link';
import { Kofi, Patreon } from './Icons';
import { SOCIALS_FOOTER } from '../lib/config';

const FooterEpisode = () => {
  return (
    <div>
      <p><b><i>Thank you playing through this episode of The Grand Challenge!</i></b></p>
      <p>
        If you <i>liked</i> this experience, please sign up for our newsletter so you can receive the latest episodes! 
        <br/><br/>

        <NewsletterSignup/>        
        <br/><br/> 

        If you <b><i>LOVED</i></b> the episode and want to join an exclusive community, 
        get behind-the-scenes content, and shape the future of the story, please sign up for a monthly subscription!
        <br/>
        <li><Link href={SOCIALS_FOOTER["Patreon"]}>Patreon <Patreon/></Link></li>
        <li><Link href={SOCIALS_FOOTER["KofiMonthly"]}>Ko-fi <Kofi/></Link></li>
        <br/>
        A current and growing list of benefits can be found here: <Link href={SOCIALS_FOOTER["OscoSubscribe"]}>{SOCIALS_FOOTER["OscoSubscribe"]}</Link>
        <br/><br/>
        <b><i>Thank you so much once again!</i></b> I hope to see you at the next episode - <i>osco</i> :3
      </p>
    </div>

  )
}

export default FooterEpisode