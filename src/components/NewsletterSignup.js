import React, { useState } from 'react'

// Component Imports
import Alert from '@mui/material/Alert';
import Button from '@mui/material/Button';
import CircularProgress from '@mui/material/CircularProgress';
import TextField from '@mui/material/TextField';

// Style Imports
import { green } from '@mui/material/colors';
import utilStyles from '../styles/utils.module.css';

export default function NewsletterSignup(){
  // Set useState() variables
  const [email, setEmail] = useState(null); 
  const [loading, setLoading] = useState(false); 
  const [loggedIn, SetLoggedIn] = useState(false); 
  const [status, setStatus] = useState(null);

  const joinNewsletter = async (e) => {
    e.preventDefault();
    setLoading(true);
    setStatus("loading")
    if (!email){ 
      alert("Please fill in an email!")
    }
    else {
      // Add User to Database if they do not exist
      console.log("--Adding user to DB...")
      console.log("email: " + email)
      await fetch("../api/post-email", {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify({"email": email}),
      });
      setStatus("complete")
      console.log("--Adding user complete!")
      SetLoggedIn(true)
    }
    setLoading(false)
  };

  return (
    <div>
        {/* Newsletter Sign */}
        <section className={`${utilStyles.headingMd} ${utilStyles.padding1px}`}>
          
          <h2 className={utilStyles.headingLg}>Sign Up to Get the Latest Episode Releases!</h2>
          { (!loggedIn) && 
            <div>
              <TextField
                disabled={loading}
                id="filled-disabled"
                label="Email"
                // defaultValue={VR_SAMPLE_PROMPT}
                variant="outlined"
                // fullWidth
                onChange={(e) => setEmail(e.target.value)}
              />
              <Button style={{ marginLeft: '20px', marginTop: '10px',  backgroundColor: green[500] }} variant="contained" color="primary" onClick={(e) => joinNewsletter(e)}>SIGN UP</Button>
            </div>
          }
          {(status === "loading") && 
            <div>
              <center>
              <br/>
              <CircularProgress />
              <br/>
              </center>
            </div>
          }
        { (status === "complete" && loggedIn) && 
          <div>
            {/* Alert  */}
            <Alert severity="success">Successfully signed up as <b>{email}</b>! Cya soon :3</Alert>
          </div>    
        }
        </section>
    </div>
  )

}