import React from "react";
import Container from "@mui/material/Container";
import Typography from "@mui/material/Typography";
import Link from "@mui/material/Link";
import Grid from "@mui/material/Grid";
import { Instagram, Reddit, Twitter, YouTube } from "@mui/icons-material";
import { Kofi, Patreon } from "./Icons";
import { Box } from "@mui/material";
import { SOCIALS_FOOTER } from "../lib/config";

export default function FooterSocials() {
  return (
    <Box
      component="footer"
      sx={{
        backgroundColor: (theme) =>
          theme.palette.mode === "light"
            ? theme.palette.grey[200]
            : theme.palette.grey[800],
        p: 6,
      }}
    >
      <Container maxWidth="lg">
        <Grid container spacing={5}>
          <Grid item>
          <Link href={SOCIALS_FOOTER["KofiMonthly"]} color="inherit">
              <Kofi/>
            </Link>
            <Link href={SOCIALS_FOOTER["Instagram"]} color="inherit" sx={{ pl: 1, pr: 1 }}>
              <Instagram />
            </Link>
            <Link href={SOCIALS_FOOTER["Patreon"]} color="inherit" sx={{ pl: 1, pr: 1 }}>
              <Patreon />
            </Link>
            <Link href={SOCIALS_FOOTER["Reddit"]} color="inherit" sx={{ pl: 1, pr: 1 }}>
              <Reddit />
            </Link>
            <Link href={SOCIALS_FOOTER["Twitter"]} color="inherit" sx={{ pl: 1, pr: 1 }}>
              <Twitter />
            </Link>
            <Link href={SOCIALS_FOOTER["YouTube"]} color="inherit">
              <YouTube />
            </Link>
          </Grid>
        </Grid>
      </Container>
    </Box>
  );
}