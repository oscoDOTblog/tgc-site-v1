// NextJS Imports
import React, { useState } from 'react';
import Head from 'next/head';
import Image from 'next/image';
import Link from 'next/link';
import Script from 'next/script'


// Config Imports
import { EPISODES, SITE_ABOUT, SITE_TITLE } from '../lib/config';

// Component Imports
import Alert from '@mui/material/Alert';
import Button from '@mui/material/Button';
import FooterSocials from '../components/FooterSocials';
import CircularProgress from '@mui/material/CircularProgress';
import Layout, { siteTitle } from '../components/layout';
import TextField from '@mui/material/TextField';

// Library Imports
import { getSortedPostsData } from '../lib/posts';

// Styling Imports
import { green } from '@mui/material/colors';
import utilStyles from '../styles/utils.module.css';

// Global Variables
const FONT_SIZE_ABOUT = '18px'
const FONT_SIZE_DATE = '16px'

export async function getStaticProps() {
  const allPostsData = getSortedPostsData();
  return {
    props: {
      allPostsData,
    },
  };
}

export default function Home({}) {
  // Set useState() variables
  const [email, setEmail] = useState(null); 
  const [loading, setLoading] = useState(false); 
  const [loggedIn, SetLoggedIn] = useState(false); 
  const [status, setStatus] = useState(null);

  const joinNewsletter = async (e) => {
    e.preventDefault();
    setLoading(true);
    setStatus("loading")
    if (!email){ 
      alert("Please fill in an email!")
    }
    else {
      // Add User to Database if they do not exist
      console.log("--Adding user to DB...")
      console.log("email: " + email)
      await fetch("../api/post-email", {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify({"email": email}),
      });
      setStatus("complete")
      console.log("--Adding user complete!")
      SetLoggedIn(true)
    }
    setLoading(false)
  };

  return (
    <div>
      <Layout home>
        <Head>
          {/* Site Title */}
          <title>{siteTitle}</title>

          {/* Plausible Tracking */}
          {/* <script defer data-domain="tgc.atos.to" src="https://plausible.atemosta.com/js/script.js"></script> */}

          {/* Open Graph Tags */}
          <meta property="og:title" content={SITE_TITLE} />
          <meta property="og:description" content="Survive the digital apocalypse"/>
          <meta property="og:image" content="/images/tgc-banner.jpg" />
          <meta property="og:url" content="https://tgc.atos.to" />
        </Head>
        <Script defer data-domain="tgc.atos.to" src="https://plausible.atemosta.com/js/script.js" />

        <section className={utilStyles.headingMd}>
          <h3><i>"The future needs you. We need you. Please hurry."</i></h3>
          <h5>On a normal day like any other, you receive an invitation to download a new game you have never heard of...</h5>
          <h5>"It can't hurt to try it out, it's not like I have anything else going on..."</h5>
          <h5>What you quickly realize, however, is that <i>this</i> is no ordinary game - you can share your thoughts and sensations with your digital character, but why?</h5>
          <h5>Your thoughts are cut short as ████████</h5>
        </section>

        {/* Latest Episodes */}
        <section className={`${utilStyles.headingMd} ${utilStyles.padding1px}`}>
          <h2 className={utilStyles.headingLg}>Latest Episodes</h2>
          <ul className={utilStyles.list}>
            {EPISODES.map(({name, date, desc, link, imageUrl}) => (
              <li className={utilStyles.listItem} key={name}>
                <div style={{ display: 'flex', alignItems: 'center' }}>
                  <div style={{ marginRight: '1rem' }}>
                    <Image src={imageUrl} alt={name} width={100} height={100} /> {/* Adjust width and height as per your requirement */}
                  </div>

                  {/* Episode Title and Link */}
                  <div>
                    <Link href={link} style={{ color: green[500] }}>
                      {/* <a target="_blank" rel="noopener noreferrer">
                        {name}

                      </a>  */}
                      {name}
                    </Link>
                    <br />

                    {/* Episode Description */}
                    <i style={{ fontSize: FONT_SIZE_ABOUT }}>{desc}</i>
                    <br/>
                    
                    {/* Episode Date */}
                    <b style={{ fontSize: FONT_SIZE_DATE }}>Release Date: {date}</b>
                  </div>
                </div>
              </li>
            ))}
          </ul>
        </section>

        {/* Newsletter Sign */}
        <section className={`${utilStyles.headingMd} ${utilStyles.padding1px}`}>
          
          <h2 className={utilStyles.headingLg}>Sign Up to Get the Latest Episode Releases!</h2>
          { (!loggedIn) && 
            <div>
              <TextField
                disabled={loading}
                id="filled-disabled"
                label="Email"
                // defaultValue={VR_SAMPLE_PROMPT}
                variant="outlined"
                // fullWidth
                onChange={(e) => setEmail(e.target.value)}
              />
              <Button style={{ marginLeft: '20px', marginTop: '10px',  backgroundColor: green[500] }} variant="contained" color="primary" onClick={(e) => joinNewsletter(e)}>SIGN UP</Button>
            </div>
          }
          {(status === "loading") && 
            <div>
              <center>
              <br/>
              <CircularProgress />
              <br/>
              </center>
            </div>
          }
        { (status === "complete" && loggedIn) && 
          <div>
            {/* Alert  */}
            <Alert severity="success">Successfully signed up as <b>{email}</b>! Cya soon :3</Alert>
          </div>    
        }
        </section>
        
        {/* Social Media Footer */}
        <section className={`${utilStyles.headingMd} ${utilStyles.padding1px}`}>
          {/* <h2 className={utilStyles.headingLg}>TODO: Social Media Footer</h2> */}
          <FooterSocials/>
        </section>

      </Layout>
    </div>
  );
}
