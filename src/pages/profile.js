// NextJS Imports
import React, { useState } from 'react';
import Head from 'next/head';
import Image from 'next/image';
import Link from 'next/link';

// Config Imports
import {BADGES_TEST, EPISODES } from '../lib/config';

// Component Imports
import Alert from '@mui/material/Alert';
import Button from '@mui/material/Button';
import CircularProgress from '@mui/material/CircularProgress';
import Date from '../components/date';
import Grid from '@mui/material/Unstable_Grid2';
import Layout from '../components/layout';
import TextField from '@mui/material/TextField';
import Tooltip from '@mui/material/Tooltip';

// Library Imports
import { getSortedPostsData } from '../lib/posts';

// Styling Imports
import utilStyles from '../styles/utils.module.css';

// Global Variables
const PAGE_TITLE = 'Profile'

export async function getStaticProps() {
  const allPostsData = getSortedPostsData();
  return {
    props: {
      allPostsData,
    },
  };
}

export default function Profile({}) {
  const [email, setEmail] = useState(null); 
  const [loading, setLoading] = useState(false); 
  const [loggedIn, SetLoggedIn] = useState(false); 
  const [status, setStatus] = useState(null);

  const joinNewsletter = async (e) => {
    e.preventDefault();
    setLoading(true);
    setStatus("loading")
    if (!email){ 
      alert("Please fill in an email!")
    }
    else {
      // Add User to Database if they do not exist
      console.log("--Adding user to DB...")
      console.log("email: " + email)
      await fetch("../api/post-email", {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify({"email": email}),
      });
      setStatus("complete")
      console.log("--Adding user complete!")
      SetLoggedIn(true)
    }
    setLoading(false)
  };


  return (
    <div>
      <Layout>
        <Head>
          <title>{PAGE_TITLE}</title>
          {/* Plausible Tracking */}
          <script defer data-domain="tgc.atos.to" src="https://plausible.atemosta.com/js/script.js"></script>
        </Head>
        {/* Page Header and Profile Login */}
        <h1>{PAGE_TITLE}</h1>

          {/* Newsletter */}
          { (!loggedIn) && 
            <div>
              <TextField
                disabled={loading}
                id="filled-disabled"
                label="Email"
                // defaultValue={VR_SAMPLE_PROMPT}
                variant="outlined"
                // fullWidth
                onChange={(e) => setEmail(e.target.value)}
              />
              <Button style={{ marginLeft: '20px', marginTop: '10px' }} variant="contained" color="primary" onClick={(e) => joinNewsletter(e)}>CREATE ACCOUNT OR LOG IN</Button>
            </div>
          }
          {(status === "loading") && 
            <div>
              <CircularProgress />
              <br/>
              <br/>
            </div>
          }

      {/* Render Badges Collected, Profile Settings, and Unlocked Content After Logging In */}
      { (status === "complete" && loggedIn) && 
        <div>
          {/* Alert  */}
          <Alert severity="success">Successfully logged in as <b>{email}</b>!</Alert>     

          {/* Profile Settings */}
          {/* <section className={`${utilStyles.headingMd} ${utilStyles.padding1px}`}>
            <h2 className={utilStyles.headingLg}>Profile Settings (COMING SOON)</h2>
          </section> */}

          {/* Badges Collected */}
          {/* <section className={`${utilStyles.headingMd} ${utilStyles.padding1px}`}>
            <h2 className={utilStyles.headingLg}>Badges Collected (COMING SOON)</h2>
            <Grid container spacing={2}>
              {BADGES_TEST.map(({ title, imageUrl }) => (
                <Grid item key={title} xs={6} sm={4} md={3}>
                  <Tooltip title={title}>
                    <div>
                      <Image src={imageUrl} alt={title} width={100} height={100} />
                    </div>
                  </Tooltip>
                </Grid>
              ))}
            </Grid>
          </section> */}

          {/* Bonus Content */}
          {/* <section className={`${utilStyles.headingMd} ${utilStyles.padding1px}`}>
            <h2 className={utilStyles.headingLg}>Bonus Content (COMING SOON)</h2>
          </section> */}
        </div>
      }
      </Layout>
    </div>
  );
}