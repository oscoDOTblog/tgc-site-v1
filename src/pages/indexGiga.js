import React from 'react';
import Head from 'next/head';
import Image from 'next/image';
import Link from 'next/link';


// Global Variables
const SITE_TITLE = "The Grand Challenge"
const AUTHOR_NAME = " oscoDOTblog"
const AUTHOR_LINK = "https://osco.blog"

export default function Landing({}) {
  return (
    <main>
        {/* Head  */}
        <Head>

          {/* Site Title */}
          <title>{SITE_TITLE}</title>

          {/* Plausible Tracking */}
          <script defer data-domain="tgc.atos.to" src="https://plausible.atemosta.com/js/script.js"></script>
        </Head>

        {/* Profile Header */}
        <div>
          <center>
            <h1>{SITE_TITLE}</h1>
            <h2>
              By 
              <Link legacyBehavior href={AUTHOR_LINK}>
                <a target="_blank" rel="noopener noreferrer">
                  {AUTHOR_NAME}
                </a> 
              </Link>
            </h2>
          </center>

          {/* Social Media Links */}
          <div>
            [TODO: ORIENT RIGHT AND REPLACE WITH LOGOS] Bluesky, Instagram, Mastodon, Twitter, YouTube, etc
          </div>

        </div>

        {/* Main Area */}
        <div>
          {/* Left Side: List of Latest Episodes */}
          <div>
            TODO: LEFT SIDE - ADD LIST OF EPISODES
          </div>

          {/* Right Side: Stats, Schedule, Plot Summary, and Button to First Episode */}
          <div>

            {/* Stats */}
            <div>

            </div>

            {/* Schedule */}
            <div>

            </div>

            {/* Plot Summary */}
            <div>

            </div>

            {/* First Episode Button */}
            <div>

            </div>
            
          </div>

        </div>

        {/* Footer */}
        {/* Add Join Newsletter Field */}
        <div>

        </div>
    </main>
  );
}
