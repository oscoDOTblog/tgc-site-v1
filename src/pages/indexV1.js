// NextJS Imports
import React from 'react';
import Head from 'next/head';
import Image from 'next/image';
import Link from 'next/link';

// Config Imports
import { EPISODES, SITE_ABOUT } from '../lib/config';

// Component Imports
import Date from '../components/date';
import Layout, { siteTitle } from '../components/layout';

// Library Imports
import { getSortedPostsData } from '../lib/posts';

// Styling Imports
import utilStyles from '../styles/utils.module.css';

// Global Variables
const FONT_SIZE_ABOUT = '18px'

export async function getStaticProps() {
  const allPostsData = getSortedPostsData();
  return {
    props: {
      allPostsData,
    },
  };
}

export default function Home({}) {
  return (
    <div>
      <Layout home>
        <Head>
          <title>{siteTitle}</title>
          {/* Plausible Tracking */}
          <script defer data-domain="tgc.atos.to" src="https://plausible.atemosta.com/js/script.js"></script>
          {/* Umami Tracking */}
          <script async defer data-website-id="91670732-5277-43e4-8a16-4d0c7cd86a0c" src="https://umami.atemosta.com/umami.js"></script>
        </Head>
        <section className={utilStyles.headingMd}>
          <h3><i>{SITE_ABOUT}</i></h3>
        </section>

        {/* Latest Episodes */}
        <section className={`${utilStyles.headingMd} ${utilStyles.padding1px}`}>
          <h2 className={utilStyles.headingLg}>Latest Episodes</h2>
          <ul className={utilStyles.list}>
            {EPISODES.map(({name, desc, link, imageUrl}) => (
              <li className={utilStyles.listItem} key={name}>
              {/* <Link legacyBehavior href={link}>
                <a target="_blank" rel="noopener noreferrer">
                  {name}
                </a> 
              </Link>
              <br/>
              <i style={{ fontSize: FONT_SIZE_ABOUT }}>{desc}</i> */}
                <div style={{ display: 'flex', alignItems: 'center' }}>
                  <div style={{ marginRight: '1rem' }}>
                    <Image src={imageUrl} alt={name} width={100} height={100} /> {/* Adjust width and height as per your requirement */}
                  </div>
                  <div>
                    <Link legacyBehavior href={link}>
                      <a target="_blank" rel="noopener noreferrer">
                        {name}
                      </a> 
                    </Link>
                    <br />
                    <i style={{ fontSize: FONT_SIZE_ABOUT }}>{desc}</i>
                  </div>
                </div>
              </li>
            ))}
          </ul>
        </section>

        {/* Profile */}
        <section className={`${utilStyles.headingMd} ${utilStyles.padding1px}`}>
          <h2 className={utilStyles.headingLg}>Profile</h2>
          <Link href={"/profile"}>
            Click here to view settings, badges, and bonus content
          </Link>
        </section>
      </Layout>
    </div>
  );
}
