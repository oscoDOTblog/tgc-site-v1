// NextJS Imports
import Head from 'next/head';

// Component Imports
import Comments from '../../components/comments';
import Date from '../../components/date';
import FooterEpisode from '../../components/FooterEpisode';
import Layout from '../../components/layout';

// Lib Imports
import { getAllPostIds, getPostData } from '../../lib/posts';

// Styling Imports
import utilStyles from '../../styles/utils.module.css';

export async function getStaticPaths() {
  const paths = getAllPostIds();
  return {
    paths,
    fallback: false,
  };
}

export async function getStaticProps({ params }) {
  // Add the "await" keyword like this:
  const postData = await getPostData(params.id);

  return {
    props: {
      postData,
    },
  };
}


export default function Post({ postData }) {
  return (
    <Layout>
      {/* Page Head */}
      <Head>
        <title>{postData.title}</title>
      </Head>
      {/* Page Content */}
      <article>
        <h1 className={utilStyles.headingXl}>{postData.title}</h1>
        <div className={utilStyles.lightText}>
          <Date dateString={postData.date} />
        </div>
        <div dangerouslySetInnerHTML={{ __html: postData.contentHtml }} />

        {/* Episode Footer with a newsletter signup button and links to Ko-fi and Patreon */}
        <FooterEpisode/>

        {/* TODO: ADD COMMENTS */}
        {/* <Comments/> */}
      </article>
    </Layout>
  );
}

