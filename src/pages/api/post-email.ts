// Next.js API route support: https://nextjs.org/docs/api-routes/introduction
import type { NextApiRequest, NextApiResponse } from 'next'
import clientPromise from '../../lib/mongodb'

// `await clientPromise` will use the default database passed in the MONGODB_URI
// However you can use another database (e.g. myDatabase) by replacing the `await clientPromise` with the following code:
//
// `const client = await clientPromise`
// `const db = client.db("myDatabase")`
//
// Then you can execute queries against your database like so:
// db.find({}) or any of the MongoDB Node Driver commands

// type Data = {
//   name: string
// }

export default async function handler(
  req: NextApiRequest,
  res: NextApiResponse,
) {
  // Parse Request Body
  const { body } = req
  const { email } = body

  // Connect to Database
  const client = await clientPromise;
  const db = client.db("TheGrandChallenge");
  let collection = await db.collection("Users");

  // Insert a document with an upsert option only if it doesn't exist
  // Add to Users Database
  let result = await collection.updateOne(
    { 'Email': email}, // Filter
    { $setOnInsert: 
      { 
        'DisplayName': "New Relay",
        'Email' : email , 
        'Newsletter' : true  
      } 
    }, // Atomic Operator
    { upsert: true } // If Set to True, Adds New Document if it doesn't already exist
  )
  // Return Result Status of User Test
  res.status(200).json(result)
}