// NextJS Imports
import React, { useState } from 'react';
import Head from 'next/head';
// import Image from 'next/image';
import Link from 'next/link';

// Config Imports
import { EPISODES, OPENINGS, SOCIALS, SUBSCRIPTIONS } from '../lib/config';

// Component Imports
import Alert from '@mui/material/Alert';
import CircularProgress from '@mui/material/CircularProgress';
import Button from '@mui/material/Button';
import Date from '../components/date';
import Layout, { siteTitle } from '../components/layout';
import TextField from '@mui/material/TextField';

// Library Imports
import { getSortedPostsData } from '../lib/posts';

// Styling Imports
import { bgWrap } from '../styles/bg.module.css'
import utilStyles from '../styles/utils.module.css';

// Global Variables
// const SITE_HEADER = "Will tomorrow come if you won't fight today?"
const FONT_SIZE_ABOUT = '18px'
const FONT_SIZE_EP_DESC = '16px'
const SITE_HEADER = "An immersive story about a mysterious virtual reality application where you play as one the last humans being hunted to extinction."

export async function getStaticProps() {
  const allPostsData = getSortedPostsData();
  return {
    props: {
      allPostsData,
    },
  };
}

export default function Home({ allPostsData }) {
  const [email, setEmail] = useState(null); 
  const [loading, setLoading] = useState(false); 
  const [loggedIn, SetLoggedIn] = useState(false); 
  const [status, setStatus] = useState(null); 

  const joinNewsletter = async (e) => {
    e.preventDefault();
    setLoading(true);
    setStatus("loading")
    if (!email){ 
      alert("Please fill in an email!")
    }
    else {
      // Add User to Database if they do not exist
      console.log("--Adding user to DB...")
      console.log("email: " + email)
      await fetch("../api/post-email", {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify({"email": email}),
      });
      setStatus("complete")
      console.log("--Adding user complete!")
      SetLoggedIn(true)
    }
    setLoading(false)
  };


  return (
    <div>
      <Layout home>
      {/* <Image
        alt="background"
        src="/bg/parasol.jpg"
        layout="fill"
        objectFit="cover"
        quality={100}
      /> */}
        <Head>
          <title>{siteTitle}</title>
          {/* Plausible Tracking */}
          <script defer data-domain="tgc.osco.blog" src="https://plausible.atemosta.com/js/script.js"></script>
          {/* Umami Tracking */}
          <script async defer data-website-id="91670732-5277-43e4-8a16-4d0c7cd86a0c" src="https://umami.atemosta.com/umami.js"></script>
        </Head>
        <section className={utilStyles.headingMd}>
          <h3><i>{SITE_HEADER}</i></h3>
          <p style={{ fontSize: FONT_SIZE_ABOUT }}>
            A man trudges along through life with an unremarkable past and no clear future. Everyday is the dreaded cycle of despair as he gets up, goes to work, returns home, and repeats the tasks endlessly. His only saving grace is virtual reality, where even just for a brief moment, he can pretend to be somewhere else.
            <br/><br/>
            Someone else.
            <br/><br/>
            Someone who matters and does things that matter.
            <br/><br/>
            Fleeting as it may, the salvation lasts just as long as the battery lasts, and once the screen turns black, he comes back again to reality and faces himself. Even this too was slowly losing its glimmer, until one day he receives a peculiar invitation for a new VR experience he does not remember signing up for…
          </p>
        </section>

        {/* Latest Openings */}
        <section className={`${utilStyles.headingMd} ${utilStyles.padding1px}`}>
          <h2 className={utilStyles.headingLg}>🎵 Latest Openings 🎵</h2>
          <ul className={utilStyles.list}>
            {OPENINGS.map(({name, desc, link}) => (
              <li className={utilStyles.listItem} key={name}>
              <Link legacyBehavior href={link}>
                <a target="_blank" rel="noopener noreferrer">
                  {name}
                </a> 
              </Link>
              <br/>
              <b>Song</b>: <i style={{ fontSize: FONT_SIZE_ABOUT }}>{desc}</i>            
              </li>
            ))}
          </ul>
        </section>

        {/* Latest Episodes */}
        <section className={`${utilStyles.headingMd} ${utilStyles.padding1px}`}>
          <h2 className={utilStyles.headingLg}>⚔️ Latest Episodes ⚔️</h2>
          <ul className={utilStyles.list}>
            {EPISODES.map(({name, desc, link}) => (
              <li className={utilStyles.listItem} key={name}>
              <Link legacyBehavior href={link}>
                <a target="_blank" rel="noopener noreferrer">
                  {name}
                </a> 
              </Link>
              <br/>
              <i style={{ fontSize: FONT_SIZE_ABOUT }}>{desc}</i>            
              </li>
            ))}
          </ul>
        </section>

        {/* Bonus Content */}
        <section className={`${utilStyles.headingMd} ${utilStyles.padding1px}`}>
          <h2 className={utilStyles.headingLg}>🤫 Bonus Content 🤫</h2>

          {/* Unlock the following content once logged in */}
          {(loggedIn) && <div>
            
            {/* Socials */}
            <br/>
            <h3 className={utilStyles.headingLg}>🐘 BONUS CONTENT: Socials 🐘</h3>
            <p><i>Watch teasers and stay up-to-date on all things TGC!</i></p>
            <ul className={utilStyles.list}>
              {SOCIALS.map(({name, desc, link}) => (
                <p key={name}>
                  <Link legacyBehavior href={link}>
                    <a target="_blank" rel="noopener noreferrer">
                      {name}
                    </a> 
                  </Link> - <b>{desc}</b>
                  {/* <CopyToClipboard className={copyStyles} text={desc} /> */}
                </p>
              ))}
            </ul>

            {/* Transcripts */}
            <br/>
            <h3 className={utilStyles.headingLg}>📖 BONUS CONTENT: Transcripts 📖</h3>
            <p><i>Read the written version of the episodes!</i></p>
            <ul className={utilStyles.list}>
              {allPostsData.map(({ id, date, title }) => (
                <li className={utilStyles.listItem} key={id}>
                  <Link href={`/posts/${id}`}>{title}</Link>
                  <br />
                  <small className={utilStyles.lightText}>
                    <Date dateString={date} />
                  </small>
                </li>
              ))}
            </ul>
            <br/>
          </div>}

          {/* Newsletter */}
          { (!loggedIn) && <div>Sign up for our newsletter and get access to behind the scenes content for upcoming episodes!<br/><br/></div>}
          {(status === "loading") && 
            <div>
              <CircularProgress />
              <br/>
              <br/>
            </div>
          }
          {(status === "complete") && 
            <div>
              <Alert severity="success">Successfully joined newsletter! We'll see you soon :3 </Alert> 
              <br/>
            </div>
          }
          <TextField
            disabled={loading}
            id="filled-disabled"
            label="Email"
            // defaultValue={VR_SAMPLE_PROMPT}
            variant="outlined"
            // fullWidth
            onChange={(e) => setEmail(e.target.value)}
          />
          <Button style={{ marginLeft: '20px', marginTop: '10px' }} variant="contained" color="primary" onClick={(e) => joinNewsletter(e)}>Join Newsletter</Button>

        {/* End of Bonus Content section */}
        </section>

        {/* Subscribe */}
        <section className={`${utilStyles.headingMd} ${utilStyles.padding1px}`}>
          <h2 className={utilStyles.headingLg}>🪙 Subscribe 🪙</h2>
          <p><i>Support TGC and gets lots of goodies for doing so!</i></p>
          <ul className={utilStyles.list}>
            {SUBSCRIPTIONS.map(({name, desc, link}) => (
              <li className={utilStyles.listItem} key={name}>
              <Link legacyBehavior href={link}>
                <b>
                <a target="_blank" rel="noopener noreferrer">
                  {name}
                </a> 
                </b>
              </Link> - {desc}
              </li>
            ))}
          </ul>

        {/* End of Subscribe section */}
        </section> 

      </Layout>
    </div>
  );
}
