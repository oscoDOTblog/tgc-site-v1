# The Grand Challenge Website (V1)
NextJS Static Site (Blog)

## Requirements
Install NVM
```
TODO
```

Install `node v20.5.1 (npm v9.8.0)` via nvm
```
nvm install v20.5.1
```

(OPTIONAL) Set current version of node as global
```
nvm alias default v20.5.1
```

## Local Deploy
### Node
```
npm i
npm run dev
```
### Docker Compose
```
docker compose build
docker compose up
```