### build environment
# pull official base image
FROM node:20.5.1 as build

# set working directory
WORKDIR /app

# add `/app/node_modules/.bin` to $PATH
ENV PATH /app/node_modules/.bin:$PATH

# install app dependencies
COPY package.json ./
COPY package-lock.json ./
RUN npm ci --silent

# add app
COPY . ./

# build app
RUN npm run build

# run App
EXPOSE 3000
CMD ["npm", "run", "start"]